<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about',[
   'as'=>'about','uses'=>'PageController@about'
]);

Route::get('/gallery',[
    'as'=>'gallery','uses'=>'PageController@gallery'
]);

Route::get('/gallery/photogallery',[
    'as'=>'gallery/photogallery','uses'=>'PageController@photogallery'
]);

Route::get('/contact',[
   'as'=>'contact','uses'=>'PageController@contact'
]);