@extends('layouts.master')

@section('title')
    About
@endsection

@section('content')

    <section class="about_wrapper">

        <div class="about_banner_wrap">
        <div class="container">
            <div class="row ">
                <div class="col-md-12">
                    <div class="banner_cover">
                        <h3 class="text-secondary font-weight-bolder banner_content">About US</h3>
                    </div>
                </div>
            </div>
        </div>
        </div>

        <div class="container">
            <div class="row mt-md-4 mt-sm-3 mt-xs-2 mb-md-2 mb-sm-1 mb-xs-1">
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="about_title">
                        {{--<h5>About US</h5>--}}
                        <p class="text-dark text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque distinctio tenetur totam. Ab animi fugiat impedit quia recusandae, reiciendis velit? Animi dolores est odio, quae reiciendis sunt ullam veritatis vitae.
                        <br>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur consequatur, deleniti doloremque error facere harum illo incidunt iure magnam magni, natus numquam odit optio, perspiciatis rerum voluptates voluptatum? Itaque, mollitia?<br>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab autem dignissimos eligendi excepturi exercitationem expedita facilis itaque laboriosam, maxime molestiae molestias nihil nobis perspiciatis possimus praesentium quisquam similique sit ut.<br>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aliquid dolores magni nemo quia reprehenderit repudiandae temporibus totam voluptas voluptatem? Aliquid dolores neque placeat rem voluptates. Consectetur molestiae non odit.
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="about_content">

                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection