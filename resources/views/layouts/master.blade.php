<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    {{--Font awesome cdn--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{asset('css/style.css')}}">



    @yield('css')
</head>
<body>
<div id="preloader">

    <div class="loader"></div>

</div>

<div id="page_wrap">
{{--TOP HEADER--}}
<div class="container-fluid top_header_wrap">
<div class="container">
    <div class="row top_header_content">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="social_status text-md-right text-sm-center">
            <ul class="list-inline">
               <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
            </ul>
        </div>
    </div>
    </div>
</div>
</div>

{{--Top Header End--}}

{{--Header--}}
<div class="container-fluid">
    <div class="container">
        <div class="row mt-md-3 mb-md-3">
            <div class="col-md-3  text-md-left mt-md-2">
                {{--<img src="" alt="Logo"/>--}}
                <h2 class="font-weight-bold"><span class="text-secondary font-size-lg">C</span><span class="" style="font-size:22px; color: lightslategray">Nepal</span></h2>
            </div>
           <div class="col-md-6 mt-md-2">
               <form class="input-group">
                   <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                   <div class="input-group-append">
                   <button class="btn btn-outline-secondary" type="submit">Search</button>
                   </div>
               </form>
           </div>

            {{--Donate Button--}}
            <div class="col-md-3 text-md-right mt-md-2">
                <a href="#" class="btn btn-outline-secondary donate_button">Donate</a>
            </div>
        </div>
    </div>
</div>

{{--Header End--}}

{{--Menu--}}


<nav class="navbar navbar-expand-lg navbar-light navbar_wrap">
    <div class="container">

                {{--<a class="navbar-brand" href="#">Navbar</a>--}}
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-md-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                About
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                <a class="dropdown-item" href="{{route('about')}}">About Us</a>
                                <a class="dropdown-item" href="#">Our Vision</a>
                                {{--<div class="dropdown-divider"></div>--}}
                                <a class="dropdown-item" href="#">Our Mission</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Project
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Project 1</a>
                                <a class="dropdown-item" href="#">Project 2</a>
                                {{--<div class="dropdown-divider"></div>--}}
                                <a class="dropdown-item" href="#">Project 3</a>
                            </div>
                        </li>


                        <li class="nav-item">
                            <a href="#" class="nav-link">Volunteer With Us</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Visit Nepal</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('gallery')}}" class="nav-link">Gallery</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Contact Us</a>
                        </li>

                    </ul>

                </div>
            </div>

            </nav>

{{--End Menu--}}

@yield('content')


{{--Footer--}}
{{--Sub footer--}}
<footer class="footer bg-light">

        <div class="container">
            <div class="row pt-3">


            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="home_about_us">
                    <h5 class="text-left mb-4">Contribution For Nepal</h5>
                    <p class="text-justify">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab amet at consectetur eum, ex fugit ipsam, iusto laudantium natus nesciunt nihil nobis odio porro quod soluta tenetur ut vel voluptatem.
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-1">
                <div class="home_page_link">
                    <h5 class="text-center mb-4">Pages</h5>
                    <ul class="list-unstyled">
                        <li class=""><a href="{{url('/')}}">Home</a></li>
                        <li class=""><a href="">About</a></li>
                        <li class=""><a href="">Contact</a></li>
                        <li class=""><a href="">Terms & Condition</a></li>
                        <li class=""><a href="">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-1">
                    <h4 class="text-center">Like Us On Facebook</h4>
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fcool.kushal&tabs=timeline&width=350&height=120&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="250" height="120" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

            </div>
            </div>
        </div>


{{--End subfooter--}}
        <div class="container-fluid bg-secondary pt-3 pb-3">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h6 class="text-center text-white">&copy; Contribution For Nepal  <?php echo date('Y'); ?></h6>
            </div>
        </div>
</footer>
{{--End Footer--}}
</div>
{{--Script--}}
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="{{asset('js/script.js')}}"></script>
@yield('script')
<script>
    jQuery('ul.navbar-nav li.dropdown').hover(function() {
        jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
    }, function() {
        jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
    });
</script>
</body>
</html>