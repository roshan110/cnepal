@extends('layouts.master')

@section('title')
    Home-Contribution For Nepal
@endsection

@section('content')

    {{--Carousel--}}
    <section class="carousel_wrap">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 p-0">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="https://ipsumimage.appspot.com/640x360" alt="First slide" height="360px" width="100%">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="https://ipsumimage.appspot.com/640x360" alt="Second slide" height="360px" width="100%">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="https://ipsumimage.appspot.com/640x360" alt="Third slide" height="360px" width="100%">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
            </div>
        </div>
    </div>
    </section>
    {{--Carousel End--}}

    {{--Project Content--}}
    <section class="project_wrap">
        <div class="container">
            {{--Project Title--}}
            <div class="project_title row mt-4 mb-2">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h3 class="text-left text-secondary">Project</h3>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 text-md-right">
                    <a href="#" class="btn btn-secondary btn-sm">See All</a>
                </div>
            </div>
            {{--Project Title Content--}}
            <div class="project_content row mb-5">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="card project_single_wrap">
                        <div class="card-body">
                    <h5 class="text-center">Project Title 1</h5>
                    <img src="http://via.placeholder.com/640x360" alt="Project Title" width="100%" height="120px">
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus cumque dicta doloremque eum illum, in iure nemo quae quidem quis, quo rem sequi sit tempora vel veritatis, vitae voluptate voluptatem.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 col-xs-12">
                    <div class="card project_single_wrap">
                        <div class="card-body">
                    <h5 class="text-center">Project Title 2</h5>
                    <img src="http://via.placeholder.com/640x360" alt="Project Title" width="100%" height="120px">
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus cumque dicta doloremque eum illum, in iure nemo quae quidem quis, quo rem sequi sit tempora vel veritatis, vitae voluptate voluptatem.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 col-xs-12 ">
                    <div class="card project_single_wrap">
                        <div class="card-body">
                    <h5 class="text-center">Project Title 3</h5>
                    <img src="http://via.placeholder.com/640x360" alt="Project Title" width="100%" height="120px">
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus cumque dicta doloremque eum illum, in iure nemo quae quidem quis, quo rem sequi sit tempora vel veritatis, vitae voluptate voluptatem.</p>
                        </div>
                    </div>
                </div>
            </div>
            {{--Project Title Content End--}}
        </div>
    </section>
    {{--Project Content End--}}

    {{--Parallax--}}
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 p-0">
            <div class="parallax home_parallax" style=" ">

            </div>
            </div>
        </div>
    </div>
    {{--Parallax End--}}

    {{--Activities Content--}}

        <div class="container">
            {{--Activities Title--}}
            <div class="activity_title row mt-md-4 mb-md-3">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h3 class="text-left text-secondary">Activities</h3>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 text-md-right">
                    <a href="#" class="btn btn-secondary btn-sm">See All</a>
                </div>
            </div>
            {{--<div class="activitiy_title row mt-2 mb-1">--}}
                {{--<div class="col-md-12">--}}
                    {{--<h3 class="text-center text-secondary">Activities</h3>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--Activities Title Content--}}
            <div class="activity-content mb-5">
            <div class="row ">
                <div class="col-lg-3 border p-1">

                    <img src="http://via.placeholder.com/640x360" alt="Project Title" width="100%" height="120px">

                </div>
                <div class="col-lg-3 border p-1">

                    <img src="http://via.placeholder.com/640x360" alt="Project Title" width="100%" height="120px">

                </div>
                <div class="col-3 border p-1">

                    <img src="http://via.placeholder.com/640x360" alt="Project Title" width="100%" height="120px">
                </div>

                <div class="col-lg-3 border p-1">

                    <img src="http://via.placeholder.com/640x360" alt="Project Title" width="100%" height="120px">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 border p-1">

                    <img src="http://via.placeholder.com/640x360" alt="Project Title" width="100%" height="120px">

                </div>
                <div class="col-lg-3 border p-1">

                    <img src="http://via.placeholder.com/640x360" alt="Project Title" width="100%" height="120px">

                </div>
                <div class="col-3 border p-1">

                    <img src="http://via.placeholder.com/640x360" alt="Project Title" width="100%" height="120px">
                </div>

                <div class="col-lg-3 border p-1">

                    <img src="http://via.placeholder.com/640x360" alt="Project Title" width="100%" height="120px">
                </div>
            </div>
            </div>
            {{--Activities  Title Content End--}}
        </div>

    {{--Activities Content End--}}




@endsection