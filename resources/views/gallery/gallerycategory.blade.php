@extends('layouts.master')

@section('title')
    Gallery
@endsection

@section('content')
    <section class="gallery_wrapper">
    <div class="about_banner_wrap">
        <div class="container">
            <div class="row ">
                <div class="col-md-12">
                    <div class="banner_cover">
                        <h3 class="text-secondary font-weight-bolder banner_content">Gallery</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row mt-md-4 mt-sm-3 mt-xs-2 mb-md-3 mb-sm-2 mb-xs-1">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card border-0">
                            <div>
                                <img class="card-img-top" src="http://wancs.org.uk/uploads/inner-bg.jpg" alt="" width="250" height="180">
                            </div>
                            <div class="card-body" style="height:70px">
                                <h4 style="text-transform:none;"><a href="{{route('gallery/photogallery')}}" class="text-secondary">Category 1</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card border-0">
                            <div>
                                <img class="card-img-top" src="http://wancs.org.uk/uploads/inner-bg.jpg" alt="" width="250" height="180">
                            </div>
                            <div class="card-body" style="height:70px">
                                <h4 style="text-transform:none;"><a href="#" class="text-secondary">Category 2</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card border-0">
                            <div>
                                <img class="card-img-top" src="http://wancs.org.uk/uploads/inner-bg.jpg" alt="" width="250" height="180">
                            </div>
                            <div class="card-body" style="height:70px">
                                <h4 style="text-transform:none;"><a href="#" class="text-secondary">Category 3</a></h4>
                            </div>
                        </div>
                    </div>


                </div>


            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">

            </div>
        </div>
    </div>
    </section>
@endsection