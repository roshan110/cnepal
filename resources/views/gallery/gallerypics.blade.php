@extends('layouts.master')

@section('title')
    Gallery | Category
@endsection

@section('content')
    <section class="gallerypic_wrapper">
        <div class="about_banner_wrap">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12">
                        <div class="banner_cover">
                            <h3 class="text-secondary font-weight-bolder banner_content">Category 1</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row mt-md-4 mt-sm-3 mt-xs-2 mb-md-4 mb-sm-3 mb-xs-2">
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12 mt-md-2 mb-md-2">
                            <div class="card border-0">
                                <div>
                                    <img class="card-img-top img-fluid" src="http://wancs.org.uk/uploads/inner-bg.jpg" alt="" width="100%" height="180">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 mt-md-2 mb-md-2">
                            <div class="card border-0">
                                <div>
                                    <img class="card-img-top img-fluid" src="http://wancs.org.uk/uploads/inner-bg.jpg" alt="" width="100%" height="180">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 mt-md-2 mb-md-2">
                            <div class="card border-0">
                                <div>
                                    <img class="card-img-top img-fluid" src="http://wancs.org.uk/uploads/inner-bg.jpg" alt="" width="100%" height="180">
                                </div>

                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6 col-xs-12 mt-md-2 mb-md-2">
                            <div class="card border-0">
                                <div>
                                    <img class="card-img-top" src="http://wancs.org.uk/uploads/inner-bg.jpg" alt="" width="250" height="180">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 mt-md-2 mb-md-2">
                            <div class="card border-0">
                                <div>
                                    <img class="card-img-top" src="http://wancs.org.uk/uploads/inner-bg.jpg" alt="" width="250" height="180">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 mt-md-2 mb-md-2">
                            <div class="card border-0">
                                <div>
                                    <img class="card-img-top" src="http://wancs.org.uk/uploads/inner-bg.jpg" alt="" width="250" height="180">
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">

                </div>
            </div>
        </div>
    </section>
@endsection